-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 30 Lis 2018, 16:01
-- Wersja serwera: 5.7.23
-- Wersja PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `userdb`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cars`
--

DROP TABLE IF EXISTS `cars`;
CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marka` text COLLATE utf8mb4_bin NOT NULL,
  `model` text COLLATE utf8mb4_bin NOT NULL,
  `opis` text COLLATE utf8mb4_bin NOT NULL,
  `cena` double NOT NULL,
  `zdjecie` text COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `login_info`
--

DROP TABLE IF EXISTS `login_info`;
CREATE TABLE IF NOT EXISTS `login_info` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `user_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `user_password` varchar(150) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Zrzut danych tabeli `login_info`
--

INSERT INTO `login_info` (`id`, `name`, `user_name`, `user_password`) VALUES
(10, 'Patryk', 'ziolek031', 'root'),
(7, 'qwe', 'ziolek', 'qwe'),
(13, 'a', 'a', 'a'),
(15, 'post', 'post', 'post');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_info`
--

DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
  `id_pesel` int(11) NOT NULL,
  `id_login` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `surname` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `city` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `adress` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `phone_number` int(11) NOT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_pesel`),
  UNIQUE KEY `id_login` (`id_login`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Zrzut danych tabeli `user_info`
--

INSERT INTO `user_info` (`id_pesel`, `id_login`, `name`, `surname`, `city`, `adress`, `phone_number`, `email`) VALUES
(12345678, 10, 'pATRYK', 'Ziolek', 'bierkucice', 'bierutowska 6', 123123, 'aaa@www'),
(543, 7, 'name2', 'ziolkowski', 'BIERUTow', 'bierutowak', 12351234, 'ziolek@asd'),
(767, 13, 'a', 'a', 'sf', 'ad', 11, 'f'),
(123, 23, 'aa', 'aa', 'za', 'dg', 1240, 'dgb');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
