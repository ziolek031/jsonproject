package com.example.ziolek.jsonproject2.databaseclass;

import com.google.gson.annotations.SerializedName;

public class Car {

    @SerializedName("id")
    int id;
    @SerializedName("marka")
    String marka;
    @SerializedName("model")
    String model;
    @SerializedName("opis")
    String opis;
    @SerializedName("cena")
    double cena;
    @SerializedName("zdjecie")
    String zdjecie;



    public int getId() {
        return id;
    }

    public String getMarka() {
        return marka;
    }

    public String getModel() {
        return model;
    }

    public String getOpis() {
        return opis;
    }

    public double getCena() {
        return cena;
    }

    public String getZdjecie() {
        return zdjecie;
    }


}
