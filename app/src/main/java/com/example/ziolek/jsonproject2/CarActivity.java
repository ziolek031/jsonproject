package com.example.ziolek.jsonproject2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;


import com.example.ziolek.jsonproject2.databaseclass.Car;

import java.util.ArrayList;

import java.util.List;


import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.example.ziolek.jsonproject2.MainActivity.service;

public class CarActivity extends AppCompatActivity {




     List<Car> carList;
     CarAdapter carAdapter;
     RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        ButterKnife.bind(this);

        recyclerView = findViewById(R.id.recyclerView);


        service.performCarInfo()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        cars -> {
                            carList = new ArrayList<>();
                            for(Car c: cars){
                                c.getMarka();
                                c.getModel();
                                c.getOpis();
                                c.getCena();
                                c.getZdjecie();
                                carList.add(c);
                            }
                            Log.d(MainActivity.TAG, "Lista: " + carList.get(1));
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            carAdapter = new CarAdapter(carList);
                            recyclerView.setAdapter(carAdapter);


  /*
                            //wersja 2
                            carList = new ArrayList<>();
                            for(int i =0; i < cars.size(); i++){
                                Car car = new Car();
                                car.setMarka(cars.get(i).getMarka());
                                car.setModel(cars.get(i).getModel());
                                car.setOpis(cars.get(i).getOpis());
                                car.setCena(cars.get(i).getCena());
                                carList.add(car);
                            }

                            carAdapter = new CarAdapter(carList);
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(CarActivity.this);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            recyclerView.setAdapter(carAdapter);
                            recyclerView.setLayoutManager(layoutManager);
                            Log.d(MainActivity.TAG, "Obiekty: " + carList);
*/


                        },
                        error -> {
                            Log.e(MainActivity.TAG, error.getMessage());
                        }
                );



    }
}
