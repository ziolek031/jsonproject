package com.example.ziolek.jsonproject2;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceConfig {


    private static final String LOG_PREF = "LOG_PREF";
    private static final String LOGIN_STATUS = "LOGIN_STAT";
    private static final String PREF_USER_LOGIN = "PREF_USER_LOGIN";
    private static final int PREF_USER_ID = 0;

    private SharedPreferences sharedPreferences;
    private Context context;

    public SharedPreferenceConfig(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(LOG_PREF,Context.MODE_PRIVATE);

    }

    public void writeLoginStatus(boolean status){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LOGIN_STATUS, status);
        editor.commit();
    }

    public boolean readLoginStatus(){
        return sharedPreferences.getBoolean(LOGIN_STATUS, false);
    }

    public void writeName(String name){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREF_USER_LOGIN,name);
        editor.commit();
    }

    public String readName(){
        return sharedPreferences.getString(PREF_USER_LOGIN,null);
    }

    public void writeId(int id){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(String.valueOf(PREF_USER_ID),id);
        editor.commit();
    }

    public int readId(){
        return sharedPreferences.getInt(String.valueOf(PREF_USER_ID),0);
    }
}

