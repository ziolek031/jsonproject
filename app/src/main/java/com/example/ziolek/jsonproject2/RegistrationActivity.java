package com.example.ziolek.jsonproject2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;



public class RegistrationActivity extends AppCompatActivity {

    private MainActivityService service;

    @BindView(R.id.txt_name)
    EditText txtName;
    @BindView(R.id.txt_user_name)
    EditText txtUserName;
    @BindView(R.id.txt_password)
    EditText txtPassword;
    @BindView(R.id.bn_register)
    Button bnRegister;

    private String name;
    private String userName;
    private String userPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        service = ApiClient.getApiClient().create(MainActivityService.class);
    }

    @OnClick(R.id.bn_register)
    public void onViewClicked() {
        name = txtName.getText().toString();
        userName = txtUserName.getText().toString();
        userPass = txtPassword.getText().toString();

        service.performRegistration(name,userName,userPass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        user -> {
                            if (user.getResponse().equals("exist")) {
                                Log.i(MainActivity.TAG, "Taki użytkownik już istnieje..." + user.getName()
                                        + " response: " + user.getResponse()
                                        + " to String: " + user.toString());
                                Toast.makeText(this, "Taki użytkownik już istnieje...", Toast.LENGTH_LONG).show();


                            } else if (user.getResponse().equals("ok")){
                                Log.i(MainActivity.TAG, "Stworzono nowego użytkownika" + user.getName()
                                        + " response: " + user.getResponse()
                                        + " to String: " + user.toString());
                                Toast.makeText(this, "Stworzono nowego użytkownika",Toast.LENGTH_LONG).show();

                            }  else if (user.getResponse().equals("error")){
                                Log.i(MainActivity.TAG, "Cos poszlo nie tak przy rejestracji" + user.getName()
                                        + " response: " + user.getResponse()
                                        + " to String: " + user.toString());
                                Toast.makeText(this, "Cos poszlo nie tak przy rejestracji",Toast.LENGTH_LONG).show();

                            }                      },
                        error -> {
                            Toast.makeText(this, "Cos poszlo nie tak przy rejestracji..." + error.getMessage(), Toast.LENGTH_LONG).show();
                        }
                );
    }
}

