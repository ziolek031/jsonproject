package com.example.ziolek.jsonproject2;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ziolek.jsonproject2.databaseclass.Car;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CarAdapter extends RecyclerView.Adapter {

    private List<Car> mCars;




//    private RecyclerView mRecyclerView;
//
//    public CarAdapter(List<Car> mCars, RecyclerView mRecyclerView) {
//        this.mCars = mCars;
//        this.mRecyclerView = mRecyclerView;
//    }
    public CarAdapter(List<Car> mCars) {
        this.mCars = mCars;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView mMarka;
        public TextView mModel;
        public TextView mOpis;
        public TextView mCena;
        public ImageView mZdjecie;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mMarka = itemView.findViewById(R.id.car_marka);
            mModel = itemView.findViewById(R.id.car_model);
            mOpis = itemView.findViewById(R.id.car_opis);
            mCena = itemView.findViewById(R.id.car_cena);
            mZdjecie = itemView.findViewById(R.id.car_imageView);
        }
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                                    .inflate(R.layout.car_layout, viewGroup, false);
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //odnajdujemy indeks kliknietego elementu
//                int positionToDelete = mRecyclerView.getChildAdapterPosition(v);
//                //usuwamy element ze zrodla danych
//                mCars.remove(positionToDelete);
//                //w animowany sposob usunie element z listy
//                notifyItemRemoved(positionToDelete);
//            }
//        });
        //tworzymy i zwracamy obiekt ViewHolder
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        //uzupelniamy layout artykulu
        Car car = mCars.get(i);


        ((MyViewHolder)viewHolder).mMarka.setText(car.getMarka());
        ((MyViewHolder)viewHolder).mModel.setText(car.getModel());
        ((MyViewHolder)viewHolder).mOpis.setText(car.getOpis());
        ((MyViewHolder)viewHolder).mCena.setText(String.valueOf(car.getCena()+" zł"));

        Picasso.get()
                .load(ApiClient.BASE_URL+car.getZdjecie()) //sklejenie adresu URL
                .into(((MyViewHolder) viewHolder).mZdjecie);

    }

    @Override
    public int getItemCount() {
        return mCars.size();
    }
}
