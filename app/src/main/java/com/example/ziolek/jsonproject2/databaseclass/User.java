package com.example.ziolek.jsonproject2.databaseclass;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id_login")
    private int id_login;
    @SerializedName("response")
    private String response;
    @SerializedName("name")
    private String name;
    @SerializedName("user_name")
    private String user_name;
    @SerializedName("surname")
    private String surname;
    @SerializedName("id_pesel")
    private int id_pesel;
    @SerializedName("city")
    private String city;
    @SerializedName("adress")
    private String adress;
    @SerializedName("phone_number")
    private int phone_number;
    @SerializedName("email")
    private String email;

    public int getId() {
        return id_login;
    }

    public String getResponse() {
        return response;
    }

    public String getName() {
        return name;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getSurname() {
        return surname;
    }

    public int getId_pesel() {
        return id_pesel;
    }

    public String getCity() {
        return city;
    }

    public String getAdress() {
        return adress;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public String getEmail() {
        return email;
    }
}

