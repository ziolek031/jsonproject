package com.example.ziolek.jsonproject2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = "RETROFIT";


    public static MainActivityService service;
    public static SharedPreferenceConfig sharedPreferences;
    private String username;
    private String password;

    @BindView(R.id.login_bn)
    Button loginBn;
    @BindView(R.id.register_txt)
    Button registerBn;
    @BindView(R.id.user_name)
    EditText userName;
    @BindView(R.id.user_pass)
    EditText userPass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPreferenceConfig(getApplicationContext());

        service = ApiClient.getApiClient().create(MainActivityService.class);
        System.out.println("SERWIS: "+ service);

        if(sharedPreferences.readLoginStatus()){
            initStartPageActyvity();
            finish();
        }

    }

    @OnClick({R.id.login_bn, R.id.register_txt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_bn: onLoginClicked();
                break;
            case R.id.register_txt: onRegisterClicked();
                break;
        }
    }



    private void onLoginClicked() {
        username = userName.getText().toString();
        password = userPass.getText().toString();

        service.performUserLogin(username, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        val -> {
                            if (!val.getResponse().equals("failed")) {
                                Log.d(TAG, "Zalogowano: " + val.getName()
                                        + " response: " + val.getResponse()
                                        + " to String: " + val.toString()
                                        + " ID: " + val.getId()
                                        + " username: " + val.getUser_name()
                                        + " surname: " + val.getSurname());
                                Toast.makeText(this, "Zalogowano: " + val.getName(), Toast.LENGTH_LONG).show();

                                initStartPageActyvity();
                                sharedPreferences.writeLoginStatus(true);
                                sharedPreferences.writeName(username);
                                sharedPreferences.writeId(val.getId());

                                finish();


                            } else {
                                Log.i(TAG, "Bledny login/hasło " + val.getName()
                                        + " response: " + val.getResponse()
                                        + " to String: " + val.toString());
                                Toast.makeText(this, "Bledny login/hasło",Toast.LENGTH_LONG).show();


                            }

                        },
                        error -> {
                            Toast.makeText(this, "Cos poszlo nie tak z logowaniem..." + error.getMessage(), Toast.LENGTH_LONG).show();

                        }
                );

    }

    private void onRegisterClicked() {
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    private void initStartPageActyvity() {
        Intent intent = new Intent(this, StartPageActivity.class);
        startActivity(intent);
    }



}
