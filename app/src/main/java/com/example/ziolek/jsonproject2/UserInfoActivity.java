package com.example.ziolek.jsonproject2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ziolek.jsonproject2.databaseclass.User;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.example.ziolek.jsonproject2.MainActivity.service;
import static com.example.ziolek.jsonproject2.MainActivity.sharedPreferences;

public class UserInfoActivity extends AppCompatActivity {


    @BindView(R.id.ET_user)
    EditText ETUser;
    @BindView(R.id.ET_surname)
    EditText ETSurname;
    @BindView(R.id.ET_id_pesel)
    EditText ETIdPesel;
    @BindView(R.id.ET_city)
    EditText ETCity;
    @BindView(R.id.ET_address)
    EditText ETAddress;
    @BindView(R.id.ET_phone_number)
    EditText ETPhoneNumber;
    @BindView(R.id.ET_email)
    EditText ETEmail;
    @BindView(R.id.btnLoginInfoEdit)
    Button btnLoginInfoEdit;
    @BindView(R.id.btnSave)
    Button btnSave;

    private String name;
    private String surname;
    private String idPesel;
    private String city;
    private String address;
    private String phoneNumber;
    private String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        ButterKnife.bind(this);

        //service = ApiClient.getApiClient().create(MainActivityService.class);

        service.performUserInfo(sharedPreferences.readName())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        user -> {

                            ETUser.setText(user.getName());
                            ETSurname.setText(user.getSurname());
                            ETCity.setText(user.getCity());
                            ETAddress.setText(user.getAdress());
                            ETEmail.setText(user.getEmail());
                            ETIdPesel.setText(Integer.toString(user.getId_pesel()));
                            ETPhoneNumber.setText(Integer.toString(user.getPhone_number()));



                            Log.d(MainActivity.TAG, " " + user.getUser_name()
                                    + " " + user.getName()
                                    + " " + user.getSurname()
                                    + " ID: " + user.getId());

                            Toast.makeText(this,"ID: "+sharedPreferences.readId(),Toast.LENGTH_LONG).show();
                        },
                        error -> {
                            Log.i(MainActivity.TAG, error.getMessage());
                        }
                );

        setEnableItems(false);

    }


    @OnClick({R.id.btnLoginInfoEdit, R.id.btnSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLoginInfoEdit:
                setEnableItems(true);

                break;
            case R.id.btnSave:
                editUserItems();
                setEnableItems(false);

                break;
        }
    }

    public void editUserItems(){
//        User user = new User();
//        user.getName();
//        user.getSurname();
//        user.getId_pesel();
//        user.getCity();
//        user.getAdress();
//        user.getPhone_number();
//        user.getEmail();

        name = ETUser.getText().toString();
        surname = ETSurname.getText().toString();
        idPesel = ETIdPesel.getText().toString();
        city = ETCity.getText().toString();
        address = ETAddress.getText().toString();
        phoneNumber = ETPhoneNumber.getText().toString();
        email = ETEmail.getText().toString();

        Log.e(MainActivity.TAG, ">>>>>ID... " +sharedPreferences.readId());
//         service.performEditUserInfo(sharedPreferences.readId(),user)
        service.performEditUserInfo(sharedPreferences.readId(),Integer.parseInt(idPesel),name,surname,city,address,Integer.parseInt(phoneNumber),email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        val -> {
                            if (val.getResponse().equals("User successfully UPDATE")) {
                                Toast.makeText(this, "User successfully UPDATE", Toast.LENGTH_LONG).show();
                            } else if (val.getResponse().equals("Failed UPDATE")) {
                                Toast.makeText(this, "Failed UPDATE",Toast.LENGTH_LONG).show();
                            } else if (val.getResponse().equals("User successfully created")) {
                                Toast.makeText(this, "User successfully created",Toast.LENGTH_LONG).show();
                            }else if (val.getResponse().equals("Failed user insert row")) {
                                Toast.makeText(this, "Failed user insert row",Toast.LENGTH_LONG).show();
                            } else if (val.getResponse().equals("Required field(s) is missing")) {
                                Toast.makeText(this, "Required field(s) is missing",Toast.LENGTH_LONG).show();
                            }

                        },
                        error -> {
                            Log.i(MainActivity.TAG, error.getMessage());
                        }
                );
    }


    public void setEnableItems(boolean enableItems){
        ETUser.setEnabled(enableItems);
        ETSurname.setEnabled(enableItems);
        ETIdPesel.setEnabled(enableItems);
        ETAddress.setEnabled(enableItems);
        ETCity.setEnabled(enableItems);
        ETPhoneNumber.setEnabled(enableItems);
        ETEmail.setEnabled(enableItems);
        btnSave.setEnabled(enableItems);

    }
}

