package com.example.ziolek.jsonproject2;

import com.example.ziolek.jsonproject2.databaseclass.Car;
import com.example.ziolek.jsonproject2.databaseclass.User;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface MainActivityService {

    //    @Headers({
//            "Accept: application/json",
//            "Content-Type: application/json"
//    })
    @GET("login.php")
    Observable<User> performUserLogin(@Query("user_name") String UserName, @Query("user_password") String UserPassword);

    @GET("register.php")
    Observable<User> performRegistration(@Query("name") String Name, @Query("user_name") String UserName, @Query("user_password") String UserPassword);

    @GET("user_info.php")
    Observable<User> performUserInfo(@Query("user_name") String UserName);

    @POST("edit_user_info.php")
    Observable<User> performEditUserInfo(@Query("id_login") int IdLogin,
                                         @Query("id_pesel") int IdPesel,
                                         @Query("name") String Name,
                                         @Query("surname") String Surname,
                                         @Query("city") String City,
                                         @Query("adress") String Address,
                                         @Query("phone_number") int PhoneNumber,
                                         @Query("email") String Email
                                         );

    /*  @POST("edit_user_info.php")
    Observable<User> performEditUserInfo(@Query("id") int IdLogin,
                                         @Body User user
    );*/

    @GET("TEST.txt")
    Observable<List<Car>> performCarInfo();

//    @GET("car_info.php")
//    Observable<List<Car>> performCarInfo();
}
