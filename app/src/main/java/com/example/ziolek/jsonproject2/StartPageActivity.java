package com.example.ziolek.jsonproject2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;



import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class StartPageActivity extends AppCompatActivity {


    private SharedPreferenceConfig sharedPreference;

    @BindView(R.id.btnLogout)
    ImageButton btnLogout;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.btnUserInfo)
    ImageButton btnUserInfo;
    @BindView(R.id.btnCars)
    ImageButton btnCars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page);
        ButterKnife.bind(this);
        sharedPreference = new SharedPreferenceConfig(getApplicationContext());
        textView.setText("Zalogowano: "+ sharedPreference.readName());
    }

    @OnClick({R.id.btnLogout, R.id.btnUserInfo, R.id.btnCars})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogout: logout();
                break;
            case R.id.btnUserInfo: userInfo();
                break;
            case R.id.btnCars: carInfo();
                break;
        }
    }

    private void carInfo() { startActivity(new Intent(this, CarActivity.class));
    }

    private void userInfo() {
        startActivity(new Intent(this, UserInfoActivity.class));
    }


    private void logout() {
        sharedPreference.writeLoginStatus(false);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }


}

